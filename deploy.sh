#/bin/bash
# This file is used by Cloudflare Pages to render and deploy the site
QUARTO_VERSION="1.0.38"
R_VERSION="4.2.1"
OLD_PWD="$(pwd)"
echo "id"
id
echo "sudo id"
sudo id
echo "os-release"
cat /etc/os-release
echo "lsb-release"
cat /etc/lsb-release
echo "docker info"
docker info
rm -rf "./quarto-${QUARTO_VERSION}-linux-amd64.tar.gz"
rm -rf "./R-${R_VERSION}.tar.gz"
wget "https://cran.r-project.org/src/base/R-4/R-${R_VERSION}.tar.gz"
#wget "https://github.com/quarto-dev/quarto-cli/releases/download/v1.0.38/quarto-${QUARTO_VERSION}-linux-amd64.tar.gz"
cd /tmp
tar -xf "${OLD_PWD}/quarto-${QUARTO_VERSION}-linux-amd64.tar.gz"
#tar -xf "${OLD_PWD}/R-${R_VERSION}.tar.gz"
cd "${OLD_PWD}"
export PATH="/tmp/quarto-${QUARTO_VERSION}/bin:$PATH"
exec "quarto" "render"
